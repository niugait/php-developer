@extends('layouts.app')

@section('title', 'Usuários')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('users-create') }}" class="btn btn-outline-secondary float-right"><i class="fas fa-plus"></i> Novo</a>
                    </div>
                    <table class="table table-hover">
                        <caption class="pl-4">Listagem de Usuários</caption>
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nome</th>
                            <th scope="col">Email</th>
                            <th scope="col">Grupo</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                        <tr>
                            <th scope="row">{{ $user->id }}</th>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->is_admin ? 'Administrador' : 'Usuário' }}</td>
                            <td>
                                <a href="{{ route('users-edit', $user->id) }}" class="btn btn-outline-secondary"><i class="fas fa-pen"></i></a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
