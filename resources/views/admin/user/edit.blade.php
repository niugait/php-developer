@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Register') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('users-update', $data->id) }}">
                                <input name="_method" type="hidden" value="PUT">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') ? old('name') : $data->name }}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') ? old('email') : $data->email }}" required @if($data->name) disabled @endif autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" @if(!$data->name) required @endif autocomplete="new-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            @if(!$data)
                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>
                            @endif

                            @if (Auth::check() && Auth::user()->is_admin)
                                <div class="form-group row">
                                    <label for="role" class="col-md-4 col-form-label text-md-right">{{ __('Role') }}</label>

                                    <div class="col-md-6">
                                        <ul class="list-group">
                                            <li class="list-group-item input-radio-check">
                                                <input class="form-check-input" type="radio" name="user_type" id="regular_user" value="regular" @if(!$data->is_admin) checked @endif>
                                                <label class="form-check-label" for="regular_user">
                                                    Usuário
                                                </label>
                                            </li>
                                            <li class="list-group-item input-radio-check">
                                                <input class="form-check-input" type="radio" name="user_type" id="admin_user" value="admin" @if($data->is_admin) checked @endif>
                                                <label class="form-check-label" for="admin_user">
                                                    Administrator
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="role" class="col-md-4 col-form-label text-md-right">{{ __('Access') }}</label>

                                    <div class="col-md-6">
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" class="custom-control-input" id="brands" name="brands" @if($data->brand_access) checked @endif>
                                            <label class="custom-control-label" for="brands">Brands</label>
                                        </div>
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" class="custom-control-input" id="categories" name="categories" @if($data->category_access) checked @endif>
                                            <label class="custom-control-label" for="categories">Categories</label>
                                        </div>
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" class="custom-control-input" id="products" name="products" @if($data->product_access) checked @endif>
                                            <label class="custom-control-label" for="products">Products</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="alert alert-danger col-md-6 offset-4" role="alert">
                                    {{ __('Administrative Users don\'t have access to these pages.') }}
                                </div>
                            @endif

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
