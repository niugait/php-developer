<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group([
    'prefix' => 'admin',
    'middleware' => 'auth.admin'
], function () {
    Route::get('/users', [App\Http\Controllers\UserController::class, 'index'])->name('users');
    Route::get('/users/create', [App\Http\Controllers\Auth\RegisterController::class, 'index'])->name('users-create');
    Route::get('/users/{id}', [App\Http\Controllers\UserController::class, 'edit'])->name('users-edit');
    Route::put('/users/{id}', [App\Http\Controllers\Auth\RegisterController::class, 'update'])->name('users-update');

    Route::post('/register', [App\Http\Controllers\Auth\RegisterController::class, 'store'])->name('store-user');
});

Route::view('/brands', 'brands')->name('brands')
    ->middleware('can:view,App\Models\Brand');
Route::view('/categories', 'categories')->name('categories')
    ->middleware('can:view,App\Models\Category');
Route::view('/products', 'products')->name('products')
    ->middleware('can:view,App\Models\Product');
