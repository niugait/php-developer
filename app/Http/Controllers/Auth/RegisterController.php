<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     * @throws ValidationException
     */
    protected function store(Request $request): RedirectResponse
    {
        $this->validator($request->all())->validate();

        $user = new User();
        $user->fill($request->only(['name', 'email']));
        $user->password = Hash::make($request->input('password_confirmation'));
        $user->save();

        if (auth()->user()->is_admin) {
            if ($request->input('user_type') == 'admin') {
                $user->is_admin = true;
            } else {
                $user->brand_access = $request->exists('brands');
                $user->category_access = $request->exists('categories');
                $user->product_access = $request->exists('products');
            }
            $user->save();
        }

        return redirect(route('users'));
    }

    protected function index(): View
    {
        return view('admin.user.register');
    }


    protected function update(Request $request, int $id): RedirectResponse
    {
        $user = User::query()->where('id', $id)->firstOrFail();
        $request->validate([
            'name' => 'required|string|max:255',
        ]);
        $user->update($request->only(['name']));
        if ($user->password) {
            $user->password = Hash::make($request->input('password'));
        }
        $user->save();

        if (auth()->user()->is_admin) {
            $user->is_admin = $request->input('user_type') == 'admin';
            $user->brand_access = false;
            $user->category_access = false;
            $user->product_access = false;

            if ($request->input('user_type') == 'regular') {
                $user->brand_access = $request->exists('brands');
                $user->category_access = $request->exists('categories');
                $user->product_access = $request->exists('products');
            }
            $user->save();
        }

        return redirect(route('users'));
    }
}
