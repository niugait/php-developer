<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\View\View;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function index(): View
    {
        return view('admin.user.list', [
            'users' => User::all()
        ]);
    }

    public function edit(int $id): View
    {
        return view('admin.user.edit', [
            'data' => User::query()->where('id', $id)->firstOrFail()
        ]);
    }
}
